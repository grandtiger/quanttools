% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/bw.R
\name{bw}
\alias{\%bw\%}
\alias{bw}
\title{Check if values in vector are between specified range}
\usage{
bw(x, rng)

x \%bw\% rng
}
\arguments{
\item{x}{vector}

\item{rng}{range}
}
\description{
Check if values in vector are between specified range
}

